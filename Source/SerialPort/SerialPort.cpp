/*
 *  tlib_SerialPort.cpp
 *  sdaJuce
 *
 *  Created by tjmitche on 19/05/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "SerialPort.h"

#if JUCE_WINDOWS
#include <windows.h>
#else
#include <IOKit/IOKitLib.h>
#include <IOKit/serial/IOSerialKeys.h>
#include <IOKit/serial/ioss.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#endif

Result SerialPort::getDevicePathList (StringArray& devicePathList)
{
    devicePathList.clear();
	#if JUCE_WINDOWS
	//This uses the NT kernel QueryDosDevice API adapted from http://www.naughter.com/enumser.html
    OSVERSIONINFOEX osvi;
    memset (&osvi, 0, sizeof (osvi));
    osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
    osvi.dwPlatformId = VER_PLATFORM_WIN32_NT;
    ULONGLONG dwlConditionMask = 0;
    VER_SET_CONDITION (dwlConditionMask, VER_PLATFORMID, VER_EQUAL);
    if (VerifyVersionInfo (&osvi, VER_PLATFORMID, dwlConditionMask))
    {
        MemoryBlock buf (4096);//start with this
        int charsRead;
        int attempt = 0;
        do
        {
            charsRead = QueryDosDevice (NULL, static_cast<LPTSTR> (buf.getData()), buf.getSize());
            if (charsRead == 0)
            {
                DWORD dwError = GetLastError();
                if (dwError == ERROR_INSUFFICIENT_BUFFER)
                {
                    ++attempt;
                    if (attempt == 10) //stop infinate buffer enlargement
                        return Result::fail ("Serial Port Error: error reading COM port list");
                    buf.setSize (buf.getSize() * attempt); //Expand the buffer and loop around again 
                }
                else
                {
                    return Result::fail ("Serial Port Error: error generating device list");
                }
            }
        }
        while (charsRead == 0);
   
        LPTSTR deviceNamesPointer = static_cast<LPTSTR> (buf.getData());
        int offset = 0;
        while (deviceNamesPointer[offset] != '\0')
        {
            String device (&(deviceNamesPointer[offset]));
            if (device.startsWith("COM") && device.getTrailingIntValue() > 0)
                devicePathList.add (device);
            //Go to next device name
            offset += device.length() + 1;
        }    
    }
	#else
    io_iterator_t serialPortIterator;
    
    CFMutableDictionaryRef classesToMatch;
    
    classesToMatch = IOServiceMatching (kIOSerialBSDServiceValue);
    if (classesToMatch == 0)
        return Result::fail ("SerialPortError: IO Services error while trying to find serial devices");
    
    CFDictionarySetValue (classesToMatch, CFSTR (kIOSerialBSDTypeKey), CFSTR (kIOSerialBSDAllTypes));
    
    kern_return_t kernResult; 
    kernResult = IOServiceGetMatchingServices (kIOMasterPortDefault, classesToMatch, &serialPortIterator);    
    if (kernResult != KERN_SUCCESS)
        return Result::fail ("SerialPortError: Kernel error while trying to find serial devices");
    
    io_object_t	deviceService;
    
    // Loop untill we have all the devices
    while ((deviceService = IOIteratorNext (serialPortIterator)))
    {
		// Get the callout device's path (/dev/cu.xxxxx).         
		CFTypeRef bsdPathAsCFString = IORegistryEntryCreateCFProperty ( deviceService,
                                                                       CFSTR(kIOCalloutDeviceKey),
                                                                       kCFAllocatorDefault, 0);
        if (bsdPathAsCFString == NULL)
            return Result::fail ("SerialPortError: Error building serial device path list");
        
        Boolean result;
        char bsdPath[MAXPATHLEN] = {'\0'};
        
        // Convert the path from a CFString to a C (NUL-terminated) string for use with the POSIX open() call.
        result = CFStringGetCString ((CFStringRef)bsdPathAsCFString,
                                     bsdPath,
                                     MAXPATHLEN, 
                                     kCFStringEncodingUTF8);
        CFRelease (bsdPathAsCFString);
        
        if (result == false)
            return Result::fail ("SerialPortError: Error building serial device path list");
        
        devicePathList.add (bsdPath);
        //printf ("Device found with BSD path: %s\n", bsdPath);
        
        // Release the io_service_t now that we are done with it.
		(void) IOObjectRelease (deviceService);
    }
    
    IOObjectRelease(serialPortIterator);	// Release the iterator.

	#endif
    return Result::ok();
}

SerialPort::SerialPort()    :   Thread("Serial Connection"), 
                                readState (false), 
                                descriptorMax (-1)
{
    startThread (6);
}

SerialPort::~SerialPort()
{
    //if any ports are open then close them!
    closeAllPorts();
    
    signalThreadShouldExit();
    if (waitForThreadToExit(500) == false) //if we exited cleanly
        stopThread(0);
}

void SerialPort::openPort (const SerialPort::SerialPortSettings& settings)
{
    StringArray validDeviceList;
    getDevicePathList (validDeviceList);
    
    if (validDeviceList.contains (settings.getPortName()) == false)
    {
        //bad port name
        jassertfalse;
        return;
    }

    ScopedLock sl (lock);
    portsToOpen.add (settings);
}

void SerialPort::closePort (const String& nameOfPortToClose)
{
    ScopedLock sl (lock);
    //for (SerialPortSettings port : openPortSettings) //VS10 doesn't support these
	for (int i = 0; i < openPortSettings.size(); ++i)
    {
        const SerialPortSettings& port (openPortSettings.getReference (i));
        if (port.getPortName() == nameOfPortToClose)
        {
            portsToClose.add (nameOfPortToClose);
            return;
        }
    }
}

void SerialPort::closeAllPorts()
{
    ScopedLock sl (lock);
    
    //for (SerialPortSettings port : openPortSettings)
    for (int i = 0; i < openPortSettings.size(); ++i)
    {
		SerialPortSettings& port (openPortSettings.getReference (i));
        portsToClose.add (port.getPortName());
    }
}

//Result SerialPort::closePort(const unsigned int portNumber)
//{
//    if (isReading())
//        return Result::fail ("SerialPortError: Cannot open or close ports while read thread is running");
//    
//    if(isPortOpen(portNumber) != false)
//    {
//        if(::close(descriptors[portNumber]) == -1)					// Close the Serial Port
//            return Result::fail ("SerialPortError: Unable to close port");
//        descriptors.set (portNumber, -1);
//    }
//    
//    return Result::ok();
//}

Result SerialPort::initialisePort (SerialPort::SerialPortSettings& settings)
{
    #if JUCE_WINDOWS
    String portName ("\\\\.\\");
    portName += settings.getPortName();
    settings.setDescriptor (CreateFile (portName.toUTF8(), // this strange arrangement means this works when the com number has more than one digit value e.g. COM10 and above https://support.microsoft.com/en-us/kb/115831
                            settings.getReadWriteAccess(),
							0,
							0,
							OPEN_EXISTING,
							FILE_ATTRIBUTE_NORMAL,
							0));

    if (settings.getDescriptor() == INVALID_HANDLE_VALUE)
	{
		DWORD error = GetLastError();
		switch(error)
		{
			case ERROR_FILE_NOT_FOUND : return Result::fail ("SerialPort Error opening port: serial port does not exist"); break;
			case ERROR_ACCESS_DENIED  : return Result::fail ("SerialPort Error opening port: serial port already in use"); break;
			default: return Result::fail ("SerialPort Error opening port"); break;
		}
    }
    DCB dcbSerialParams = {0};
    dcbSerialParams.DCBlength = sizeof (dcbSerialParams);

    if ( ! GetCommState (settings.getDescriptor(), &dcbSerialParams)) 
    {
        releasePort (settings);
        return Result::fail ("SerialPortError: Unable to open port: can't set port settings (timeouts)");
    }

    dcbSerialParams.BaudRate = settings.getBaudRate();
    dcbSerialParams.ByteSize = (char)settings.getDataBits();
	dcbSerialParams.StopBits = (char)settings.getStopBits();
    dcbSerialParams.Parity = (char)settings.getParity();
	if ( ! SetCommState (settings.getDescriptor(), &dcbSerialParams))
	{
        releasePort (settings);
        return Result::fail ("SerialPortError: error setting serial port settings");
	}

    /*
	    Setting ReadIntervalTimeout to MAXDWORD and both ReadTotalTimeoutConstant and ReadTotalTimeoutMultiplier 
		to zero will cause any read operations to return immediately with whatever characters are in the buffer 
		(ie, have already been received), even if there aren�t any.
	*/
	COMMTIMEOUTS timeouts = {0};
	timeouts.ReadIntervalTimeout = MAXDWORD;//read timeout between characters in milliseconds
	timeouts.ReadTotalTimeoutConstant = 0;  //read timeout in full
	timeouts.ReadTotalTimeoutMultiplier = 0;//additional time?

	timeouts.WriteTotalTimeoutConstant = 0;	
	timeouts.WriteTotalTimeoutMultiplier = 0;
	
	
	if( ! SetCommTimeouts (settings.getDescriptor(), &timeouts))
	{
        releasePort (settings);
        return Result::fail ("SerialPortError: Unable to open port: can't set port settings (timeouts)");
	}

	#else
    settings.setDescriptor (::open (settings.getPortName().toUTF8(), settings.getReadWriteAccess() | O_NOCTTY));
    if (settings.getDescriptor() == -1) //did this fail?
        return Result::fail ("SerialPortError: Unable to open port");
    //---------
    //    // don't allow multiple opens
    //    if (ioctl(descriptor, TIOCEXCL) == -1)
    //    {
    //        //DBG("SerialPort::open : ioctl error, non critical");
    //        closePort();
    //        return OpenPortError;
    //    }
    
    //
    //    if (fcntl(descriptor, F_SETFL, 0) == -1)
    //    {
    //        //DBG("SerialPort::open : fcntl error");
    //		closePort();
    //        return OpenPortError;
    //    }
    
    //    struct termios options;
    //    memset(&options, 0, sizeof(struct termios));
    //    if(tcgetattr(portDescriptor, &options) == -1)	//Get the current options for the port...
    //    {
    //        //DBG("SerialPort::open : can't get port settings to set timeouts");
    //        closePort();
    //        return OpenPortError;
    //    }
    
    struct termios options;
    memset (&options, 0, sizeof(struct termios));
    
    //non canocal, 0.5 second timeout, read returns as soon as any data is recieved
    cfmakeraw (&options);
    options.c_cc[VMIN] = 1;     //wait for atleast 1 character - test to see if this can be set to 0 for instant return?
    options.c_cc[VTIME] = 5;    //timeout after 5 tenths of a second
    options.c_cflag |= CREAD;   //enable reciever (daft) - maybe only set this when the Read flag is set in the settings
    options.c_cflag |= CLOCAL;  //don't monitor modem control lines - set local mode
    
    //baud and bits
    cfsetspeed (&options, settings.getBaudRate());
    //cfsetispeed(&options, baudRate);	//set the in speed
    //cfsetospeed(&options, baudRate);	//set the out speed
    
    //NUMBER OF BITS
    options.c_cflag |= settings.getDataBits();
    
    //NO PARITY
    //options.c_cflag &= ~PARENB;				//Turn parity off (8N1):
    //EVEN PARITY
    //options.c_cflag |= PARENB;
    //ODD PARITY
    //options.c_cflag |= PARENB;
    //options.c_cflag |= PARODD;
    
    //STOP
    //options.c_cflag &= ~CSTOPB;         //disable 2 stop bits
    
    //character size mask??
    //options.c_cflag &= ~CSIZE;
    
    //FLOWCONTROL XONXOFF:
    //options.c_iflag |= IXON;
    //options.c_iflag |= IXOFF;
    
    //FLOWCONTROL_HARDWARE:
    //options.c_cflag |= CCTS_OFLOW;
    //options.c_cflag |= CRTS_IFLOW;
    //options.c_cflag |= CRTSCTS;    // enable hardware flow control
    options.c_cflag &= ~CRTSCTS;	//dissable hardware flow control
    
    //options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);	//recieve unprocessed raw input
    //options.c_iflag &= ~(IXON | IXOFF | IXANY);	// enabled software flow control
    //options.c_iflag &= ~(IXON | IXOFF | IXANY);	//dissable software flow control
    
    //set to non-standard baudRate
    if (tcsetattr (settings.getDescriptor(), TCSAFLUSH, &options) == -1)
    {
        releasePort (settings);
        return Result::fail ("SerialPortError: Unable to open port: can't set port settings (timeouts)");
    }
    
    //----------------
    //    if (tcsetattr(descriptors[1], TCSAFLUSH, &options) == -1)
    //    {
    //        //DBG("SerialPort::open : can't set port settings (timeouts)");
    //		closePort();
    //        return OpenPortError;
    //    }
    //    if (tcsetattr(descriptors[2], TCSAFLUSH, &options) == -1)
    //    {
    //        //DBG("SerialPort::open : can't set port settings (timeouts)");
    //		closePort();
    //        return OpenPortError;
    //    }
    //----------------
    
    if (tcsetattr (settings.getDescriptor(),TCSANOW,&options) == -1)
    {
        std::cout << "SerialPort::open : can't set port settings (timeouts)";
        releasePort (settings);
        return Result::fail ("SerialPortError: Unable to open port");
    }
    
    //-------------
    //    if (tcsetattr(descriptors[1],TCSANOW,&options) == -1)
    //    {
    //        //DBG("SerialPort::open : can't set port settings (timeouts)");
    //		closePort();
    //        return OpenPortError;
    //    }
    //    if (tcsetattr(descriptors[2],TCSANOW,&options) == -1)
    //    {
    //        //DBG("SerialPort::open : can't set port settings (timeouts)");
    //		closePort();
    //        return OpenPortError;
    //    }
    //-------------
    
    //    descriptorMax = -1;
    //    for (int i = 0; i > descriptors.size(); i++)
    //    {
    //        if (descriptors[i] > descriptorMax)
    //            descriptorMax = descriptors[i];
    //    }
    
    
    //set non standard baud rate
    //    speed_t nonStandardBaud = 250000;
    //    if (ioctl(descriptors.getLast(), IOSSIOSPEED, &nonStandardBaud, 1) == -1)
    //    {
    //        std::cout << "SerialPort::open : can't set non-standard speed";
    //        closePort(descriptors.size()-1);
    //        return OpenPortError;
    //    }
    #endif
    return Result::ok();//success
}

Result SerialPort::releasePort (SerialPort::SerialPortSettings& settings)
{
    #if JUCE_WINDOWS
    if (CloseHandle (settings.getDescriptor()) == 0)
    #else
    if (::close (settings.getDescriptor()) == -1)					// Close the Serial Port
    #endif
    return Result::fail ("SerialPortError: Unable to close port");
    
    return Result::ok();
}

bool SerialPort::isReading() const
{ 
    return isThreadRunning();
}

//bool SerialPort::isPortOpen(const unsigned int portNumber) const
//{
//    if (portNumber < descriptors.size() && descriptors[portNumber] != -1) 
//        return true;
//
//    return false;
//}

void SerialPort::writeToPort (const String& nameOfPortToWriteTo, const char* const data, unsigned int dataSize)
{
    ScopedLock sl (lock);
    dataToWrite.add (WriteData (nameOfPortToWriteTo, data, dataSize));
}

void SerialPort::addListener (SerialPort::Listener* const listenerToAdd)
{
    if (listenerToAdd != nullptr) 
    {
        ScopedLock sl (lock);
        listeners.add (listenerToAdd);
    }
}

void SerialPort::removeListener (SerialPort::Listener* const listenerToRemove)
{
    if (listenerToRemove != nullptr) 
    {
        ScopedLock sl (lock);
        listeners.remove (listenerToRemove);
    }
}

void SerialPort::run()
{
    DBG ("Running the SerialPort read thread");
    while ( ! threadShouldExit()) 
    {
        if (readState)
            waitAndRead();
        else
            sleep (5);

        writeQueuedMessages();
        
        //process any queued / open close requests
        configurePortsToOpen();
        configurePortsToClose();
    }
    //read thread will now be inactive.
    DBG ("RunThreadInactive Size of Ports open:" << openPortSettings.size());

}

void SerialPort::waitAndRead()
{
    unsigned char dataByte;
    Result error = Result::ok();

    #if JUCE_WINDOWS

    DWORD bytesRead = 0;
	SerialPortSettings port (openPortSettings.getUnchecked (0));
    if( ! ReadFile (port.getDescriptor(), &dataByte, 1, &bytesRead, NULL))
	{
		error = Result::fail("SerialPortError: Error reading from the serial input");
	}
	else
	{
		if (bytesRead)
		{
		    ScopedLock sl (lock);
            listeners.call (&Listener::serialDataRecieved, port.getPortName(), dataByte);
		}
		else 
		{
			sleep (5);
		}
	}

    #else
    
    ////////////////////////////////////
    /* Do the select */
    struct timeval timeout;
    timeout.tv_sec  = 0;
    timeout.tv_usec = 5000;
    fd_set  descriptorSet;
    FD_ZERO (&descriptorSet);
    for (SerialPortSettings port : openPortSettings)
    {
        FD_SET (port.getDescriptor(), &descriptorSet);
    }
    int ret  = select (descriptorMax, &descriptorSet, NULL, NULL, &timeout);
    
    if (ret < 0)
    {
        error = Result::fail("SerialPortError: An error occurred, waiting for port activity (select() error)");
    }
    else if (ret == 0)
    {
        error = Result::fail ("SerialPortError: Serial port timeout - no serial port activity");
    }
    else //select worked fine
    {
        for (SerialPortSettings port : openPortSettings)
        {
            // If we have input then read it
            if (FD_ISSET (port.getDescriptor(), &descriptorSet))
            {
                int readSize = read (port.getDescriptor(), &dataByte , 1);
                if (readSize != 1) //if it didn't work
                {
                    error = Result::fail("SerialPortError: Error reading from the serial input");
                }
                else
                {
                    ScopedLock sl (lock);
                    listeners.call (&Listener::serialDataRecieved, port.getPortName(), dataByte);
                }
            }
            else
            {
                error = Result::fail("SerialPortError: Read data not available - (select() error?)");
            }
        }
    }
    //do something with the error?
    #endif  
}

void SerialPort::writeQueuedMessages()
{
    Result error = Result::ok();
    ScopedLock sl (lock);
    while (dataToWrite.size())
    {
        WriteData writeData = dataToWrite.getFirst();
        
        for (int i = 0; i < openPortSettings.size(); i++)
        {
            SerialPortSettings port = openPortSettings.getUnchecked (i);
            if (port.getPortName() == writeData.portToWriteTo)
            {
                MemoryBlock& mb (writeData.dataToWrite);

                #if JUCE_WINDOWS
                unsigned long bytesWritten;
                if (WriteFile (port.getDescriptor(), mb.getData(), mb.getSize(), &bytesWritten, NULL) == false)//failed
                {
                    error = Result::fail ("SerialPortError: Writing to the Serial Port Failed");
                }
                else if (bytesWritten != mb.getSize())
                {
                    error = Result::fail ("SerialPortError: Writing to the Serial Port Failed");
                }
                #else
                if ( write (port.getDescriptor(), (char*)mb.getData(), mb.getSize()) != mb.getSize());
                    error = Result::fail ("SerialPortError: Writing to the Serial Port Failed");
                #endif
             }
        }
        dataToWrite.remove (0);
    }
}

void SerialPort::configurePortsToOpen()
{
    ScopedLock sl (lock);
    while (portsToOpen.size())
    {
        SerialPortSettings port = portsToOpen.getLast();
        if (initialisePort (port).wasOk())
        {
            DBG ("Opening Port: " << port.getPortName());
            openPortSettings.add (port);
            configureGlobalSettings();
        }
        portsToOpen.removeLast();
    }
}

void SerialPort::configurePortsToClose()
{
    ScopedLock sl (lock);
    while (portsToClose.size())
    {
        String portNameToClose = portsToClose.getLast();
        
        for (int i = 0; i < openPortSettings.size(); i++)
        {
            SerialPortSettings port = openPortSettings.getUnchecked (i);
            if (port.getPortName() == portNameToClose)
            {
                DBG ("Releasing Port: " << port.getPortName());
                releasePort (port);
                openPortSettings.remove (i);
                configureGlobalSettings();
                break;
            }
        }
        portsToClose.removeLast();
    }
}

void SerialPort::configureGlobalSettings()
{   
    #if JUCE_MAC
    descriptorMax = -1;
    for (SerialPortSettings port : openPortSettings)
    {
        if (port.getDescriptor() > descriptorMax)
            descriptorMax = port.getDescriptor();
    }
    descriptorMax += 1;
    #endif
    
    readState = false;
    for (int i = 0; i < openPortSettings.size(); i++)
    {
        SerialPortSettings& port = openPortSettings.getReference (i);
        if (port.getReadWriteAccess() == ReadOnly || port.getReadWriteAccess() == ReadAndWrite)
            readState = true;
    }
}
