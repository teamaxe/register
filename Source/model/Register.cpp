/*
  ==============================================================================

    Register.cpp
    Created: 14 Aug 2015 1:04:33am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Register.h"

const Identifier Student::Ids::STUDENT                  ("STUDENT");
const Identifier Student::Ids::studentKey               ("Key");
const Identifier Student::Ids::studentName              ("Name");
const Identifier Student::Ids::studentNumber            ("Number");
const Identifier Student::Ids::studentEmail             ("Email");

const Identifier Student::Ids::ATTENDANCERECORD         ("ATTENDANCERECORD");
const Identifier Student::Ids::ATTENDANCE               ("ATTENDANCE");
const Identifier Student::Ids::Time                     ("Time");


