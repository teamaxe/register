/*
  ==============================================================================

    Register.h
    Created: 14 Aug 2015 1:04:33am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"

#ifndef REGISTER_H_INCLUDED
#define REGISTER_H_INCLUDED

/** Model/Controller class for a student*/
struct Student
{
    struct Ids
    {
        static const Identifier STUDENT;             // root node
        static const Identifier studentKey;          //
        static const Identifier studentName;         //
        static const Identifier studentNumber;       //
        static const Identifier studentEmail;        //
        
        static const Identifier ATTENDANCERECORD;
        static const Identifier ATTENDANCE;
        static const Identifier Time;
    };
    
    /** construct */
    Student() {}
    Student (ValueTree tree)
    {
        if (tree.getType() == Ids::STUDENT)
            studentTree = tree;
    }
    Student (const String& key, const String& name, const String& number, const String& email)
    :   studentTree (Ids::STUDENT)
    {
        studentTree.setProperty (Ids::studentKey, key, nullptr);
        studentTree.setProperty (Ids::studentName, name, nullptr);
        studentTree.setProperty (Ids::studentNumber, number, nullptr);
        studentTree.setProperty (Ids::studentEmail, email, nullptr);
    }
    /** destruct */
    ~Student() {}
    
    void registerAttendance()
    {
        Time currentTime = Time::getCurrentTime();
        ValueTree attendanceRecord (studentTree.getOrCreateChildWithName (Ids::ATTENDANCERECORD, nullptr));
        
        ValueTree lastEntry (attendanceRecord.getChild (attendanceRecord.getNumChildren() - 1));
        if (lastEntry.isValid())
        {
            Time lastTime = Time (lastEntry.getProperty (Ids::Time));
            if (currentTime.getDayOfYear() == lastTime.getDayOfYear())
            {
                String messageText (String (getName() + " has already swiped in today"));
                AlertWindow::showMessageBoxAsync (AlertWindow::WarningIcon, "Already swiped in...", messageText, "OK");
                return;
            }
        }
        ValueTree entry (Ids::ATTENDANCE);
        entry.setProperty (Ids::Time, currentTime.toMilliseconds(), nullptr);
        attendanceRecord.addChild(entry, -1, nullptr);
        String messageText (String ("Thank You for Signing In:") + String (getName()) + " :)");
        AlertWindow::showMessageBoxAsync (AlertWindow::InfoIcon, "Done", messageText, "OK");
    }
    
    String getKey() const {return studentTree.getProperty (Ids::studentKey);}
    String getName() const {return studentTree.getProperty (Ids::studentName);}
    String getNumber() const {return studentTree.getProperty (Ids::studentNumber);}
    String getEmail() const {return studentTree.getProperty (Ids::studentEmail);}
    
    bool getAttendance (const int weekTimeStart, const int weekTimeEnd)
    {
        ValueTree attendanceRecord (studentTree.getOrCreateChildWithName (Ids::ATTENDANCERECORD, nullptr));
        const int numChildren = attendanceRecord.getNumChildren();
        for (int i = 0; i < numChildren; i++) {
            int value = attendanceRecord.getChild(i).getProperty(Ids::ATTENDANCE);
            if (value >= weekTimeStart && value < weekTimeEnd)
            {
                return true;
            }
        }
        return false;
    }
    bool isValid() const {return studentTree.isValid();}
    
    static ValueTree createStudentTree (const String& key, const String& name, const String& number, const String& email)
    {
        Student s (key, name, number, email);
        return s.studentTree;
    };
    
    ValueTree studentTree;
};

class StudentConfirmationThread  : public ThreadWithProgressWindow
{
public:
    StudentConfirmationThread (ValueTree registerTree_, const Student student_, Atomic<int>& confirming_)
     :  ThreadWithProgressWindow ("Confirm new entry...", true, true),
        registerTree (registerTree_),
        student (student_),
        done (false),
        confirming (confirming_)
    {
        confirming = true;
        setStatusMessage ("Swipe again to confirm:" + student.getName());
    }
    
    void run() override
    {
        setProgress (-1.0); // setting a value beyond the range 0 -> 1 will show a spinning bar..
        
        while ( ! done.get())
        {
            wait (10);
        }
    }
    
    void processKey (const String& secondKey_)
    {
        secondKey = secondKey_;
        done = true;
    }
    
    // This method gets called on the message thread once our thread has finished..
    void threadComplete (bool userPressedCancel) override
    {
        if (userPressedCancel)
        {
            AlertWindow::showMessageBoxAsync (AlertWindow::WarningIcon,
                                              "Progress window",
                                              "You pressed cancel!");
        }
        else
        {
            if (secondKey == student.getKey())
            {
                registerTree.addChild (student.studentTree, -1, nullptr);
                student.registerAttendance();
                String messageText (String (student.getName() + " added to register"));
                AlertWindow::showMessageBoxAsync (AlertWindow::InfoIcon, "Confirmed", messageText, "OK");
                
            }
            else //this is a second swipe but a different key
            {
                AlertWindow::showMessageBoxAsync (AlertWindow::WarningIcon, "Second swipe missmatch",
                                                  "Confirmation swipe does not match",
                                                  "OK");
            }
        }
        confirming = false;
        delete this;
    }
private:
    ValueTree registerTree;
    Student student;
    Atomic<int> done;
    Atomic<int>& confirming;
    String secondKey;
};


/** Model/Controller class for the register*/
class Register : public Timer
{
public:
    /** construct */
    Register() : confirming (false)
    {
        // load previous settings if possible
        File appFolder (File::getSpecialLocation (File::currentExecutableFile)
                                .getParentDirectory()
                                .getParentDirectory()
                                .getParentDirectory()
                                .getParentDirectory());
        
        appFolder.createDirectory();
        
        File settingsFile (appFolder.getChildFile("settings.xml"));
        
        if (settingsFile.existsAsFile())
        {
            XmlDocument settingsDoc (settingsFile);
            ScopedPointer<XmlElement> settingsXML (settingsDoc.getDocumentElement());
            
            registerTree = ValueTree::fromXml (*settingsXML);
        }
        else
        {
            registerTree = ValueTree ("REGISTER");
        }
        
        if ( ! registerTree.isValid())
        {
            DBG ("Register Tree is Invalid");
        }
        startTimer(10000); // 10 sec auto save
    }
    /** destruct */
    ~Register()
    {
        saveRegisterToFile();
    }
    
    void timerCallback()
    {
        saveRegisterToFile();
    }

    
    void saveRegisterToFile () {
        File appFolder (File::getSpecialLocation (File::currentExecutableFile)
                        .getParentDirectory()
                        .getParentDirectory()
                        .getParentDirectory()
                        .getParentDirectory());
        File settingsFile (appFolder.getChildFile ("settings.xml"));
        
        if (! settingsFile.existsAsFile())
            settingsFile.create();
        
        ScopedPointer<XmlElement> settingsXml (registerTree.createXml());
        if (settingsXml->writeToFile (settingsFile, ""))
            DBG("XML File written");
        else
            DBG("Error saving settings");
        
        DBG (registerTree.toXmlString());
    }
    
    /** This registers a student's id key and logs the time next to an existing entry or envokes the creation of a new entry (if this student does not already exist on the system) */
    void processKey (const String& key)
    {
        if ( ! keyIsValid (key))
            return;
        
        ValueTree studentTree = registerTree.getChildWithProperty (Student::Ids::studentKey, key);
        if (studentTree.isValid()) //existing student
        {
            Student student (studentTree);
            student.registerAttendance();
        }
        else if ( ! confirming.get())//this is the first swipe of a new entry
        {
            AlertWindow w ("Unknown student", "Info required:", AlertWindow::QuestionIcon);
            
            w.addTextEditor ("name", "enter your name here", "Name");
            w.addTextEditor ("number", "enter your student number here", "Student Number");
            w.addTextEditor ("email", "enter your email here", "Email Address");
            
            w.addButton ("OK",     1, KeyPress (KeyPress::returnKey, 0, 0));
            w.addButton ("Cancel", 0, KeyPress (KeyPress::escapeKey, 0, 0));
            
            if (w.runModalLoop() != 0) // they clicked 'ok'
            {
                String name = w.getTextEditorContents ("name");
                String number = w.getTextEditorContents ("number");
                String email = w.getTextEditorContents ("email");

                (studentConfirmationThread = new StudentConfirmationThread
                                               (registerTree,
                                                Student (key, name, number, email),
                                                confirming))->launchThread();
            }
        }
        else if (confirming.get())
        {
            studentConfirmationThread->processKey (key);
        }
    }
    
    void exportReadibleRegister ()
    {
        DBG ("Export Register");
        FileChooser fc ("Choose a directory...",
                        File::getSpecialLocation (File::userDesktopDirectory),
                        "*",
                        true);
        
        if (fc.browseForDirectory())
        {
            File chosenDirectory = fc.getResult();
            
            File outFile (chosenDirectory.getChildFile ("registerExport.csv"));
            
            if (outFile.exists())
                outFile.deleteFile();
            
            ScopedPointer<FileOutputStream> registerStream (outFile.createOutputStream());
            
            if (registerStream != nullptr)
            {
                registerStream->writeString ("UFCFF4-30-1\n");
                
                registerStream->writeString ("Name");
                registerStream->writeByte (',');
                registerStream->writeString ("Number");
                registerStream->writeByte (',');
                registerStream->writeString ("Sessions Attended");
                registerStream->writeByte ('\n');

                for (int i = 0; i < registerTree.getNumChildren(); i++)
                {
                    ValueTree studentTree (registerTree.getChild(i));
                    registerStream->writeString (studentTree.getProperty (Student::Ids::studentName).toString());
                    registerStream->writeByte (',');
                    registerStream->writeString (studentTree.getProperty (Student::Ids::studentNumber).toString());
                    registerStream->writeByte (',');
                    registerStream->writeString (String (studentTree.getChildWithName (Student::Ids::ATTENDANCERECORD).getNumChildren()));
                    registerStream->writeByte ('\n');
                }
                registerStream = nullptr;
            }
        }
    }
    
    /** Returns the entire register value tree */
    ValueTree& getValueTree()
    {
        return registerTree;
    }
    
    int64 getMinAttendanceTime ()
    {
        
        int64 min = Time::getCurrentTime().toMilliseconds();
        for (int i = 0; i < registerTree.getNumChildren(); i++)
        {
            ValueTree studentTree (registerTree.getChild(i));
            ValueTree attendanceTree = studentTree.getChildWithName (Student::Ids::ATTENDANCERECORD);
            for (int ti = 0; ti < attendanceTree.getNumChildren(); ti++) {
            int64 time = attendanceTree.getChild(i).getProperty(Student::Ids::Time, Time::getCurrentTime().toMilliseconds());
                min = time < min ? time : min;
            }
        }
        return min;
    }
    int64 getMaxAttendanceTime ()
    {
        int64 max = 0;
        for (int i = 0; i < registerTree.getNumChildren(); i++)
        {
            ValueTree studentTree (registerTree.getChild(i));
            ValueTree attendanceTree = studentTree.getChildWithName (Student::Ids::ATTENDANCERECORD);
            for (int ti = 0; ti < attendanceTree.getNumChildren(); ti++) {
                int64 time = attendanceTree.getChild(i).getProperty(Student::Ids::Time, Time::getCurrentTime().toMilliseconds());
                max = time > max ? time : max;
            }
        }
        return max;
    }
 
private:
    /** Need to build this up after looking at the keys in more detail */
    bool keyIsValid (const String& key)
    {
        return ! (key.isEmpty() || key == "Start");
    }
    
    ValueTree registerTree;
    StudentConfirmationThread* studentConfirmationThread;
    Atomic<int> confirming;

};

#endif  // REGISTER_H_INCLUDED
