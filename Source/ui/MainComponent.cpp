/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

class KeyMessage : public Message
{
public:
    KeyMessage (const String& key_) : key (key_) {}
    const String key;
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (KeyMessage);
};

//==============================================================================
MainComponent::MainComponent (Register& studentRegister_)
 :  studentRegister (studentRegister_),
    serialControls (serialPort),
    registerComponent (studentRegister_)

{
    setSize (1024, 600);
    addAndMakeVisible (serialControls);
    
    addAndMakeVisible (registerComponent);
    
    feedbackWindow.setMultiLine (true);
    feedbackWindow.setReadOnly (true);
    feedbackWindow.setFont (Font (21));
    feedbackWindow.setText ("Some text");
    feedbackWindow.setCaretVisible (false);
    addAndMakeVisible (feedbackWindow);

    buffer.preallocateBytes (1024);
    
    serialPort.addListener (this);
}

MainComponent::~MainComponent()
{
    serialPort.removeListener (this);
}

void MainComponent::resized()
{
    Rectangle<int> r (getLocalBounds());
    serialControls.setBounds (r.removeFromTop (20));
    feedbackWindow.setBounds (r.removeFromBottom (40));
    registerComponent.setBounds (r);
}

void MainComponent::serialDataRecieved (const String& /*portName*/, const uint8 byte)
{
    if ((char)byte == '\n') //
    {
        buffer = buffer.trimEnd();
        DBG (buffer);
        postMessage (new KeyMessage (buffer));
        buffer.clear();
    }
    else
    {
        buffer += (char)byte;
    }
}

void MainComponent::handleMessage (const Message& message)
{
    const KeyMessage& mes = dynamic_cast<const KeyMessage&> (message);
    studentRegister.processKey (mes.key);
}
//Menu
//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(ExportRegister, "Export Register", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == ExportRegister)
        {
            studentRegister.exportReadibleRegister();
        }
    }
}

