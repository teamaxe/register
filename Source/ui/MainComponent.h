/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"

#include "../model/Register.h"
#include "../SerialPort/SerialPort.h"

#include "SerialControls.h"
#include "RegisterComponent.h"

//==============================================================================
/* The top most application component */
class MainComponent :   public  Component,
                        private SerialPort::Listener,
                        private MessageListener,
                        public MenuBarModel
{
public:
    //==============================================================================
    MainComponent (Register& studentRegister_);
    /** destruct */
    ~MainComponent();

    //Component
    void resized() override;
    //SerialPort::Listener
    void serialDataRecieved (const String& /*portName*/, const uint8 byte) override;
    void serialConnectionError(const unsigned int errorCode) override {};
    // Called when the connection timesout
    void serialPortTimeout() override {};

    //MessageListener
    void handleMessage (const Message& message);
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        ExportRegister = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;

private:
    Register& studentRegister;
    SerialPort serialPort;
    String buffer;

    SerialControls serialControls;
    RegisterComponent registerComponent;
    TextEditor feedbackWindow;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
