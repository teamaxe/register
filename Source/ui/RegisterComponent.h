/*
  ==============================================================================

    RegisterComponent.h
    Created: 19 Aug 2015 10:21:58am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef REGISTERCOMPONENT_H_INCLUDED
#define REGISTERCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../model/Register.h"


/** The table */
class RegisterTable :   public Component,
                        public TableListBoxModel
{
public:
    static const int64 timeInAWeek = 604800 * 1000; // http://www.epochconverter.com/

    /** construct */
    RegisterTable (Register& studentRegister_) : studentRegister (studentRegister_)
    {
        addAndMakeVisible (registerTable);
        registerTable.setHeaderHeight (20);
        registerTable.setModel (this);
        // give it a border
        registerTable.setColour (ListBox::outlineColourId, Colours::grey);
        registerTable.setOutlineThickness (1);
        
        // Add some columns to the table header, based on a dummy student entry..
        Student temp ("", "", "", "");
        for (int i = 0; i < temp.studentTree.getNumProperties(); ++i)
        {
            registerTable.getHeader().addColumn
                (temp.studentTree.getPropertyName (i).toString(), i + 1, 50);
        }
        registerTable.getHeader().addColumn ("Attendance", registerTable.getHeader().getNumColumns(false) + 1, 500);
        registerTable.getHeader().addColumn ("Totals", registerTable.getHeader().getNumColumns(false) + 1, 50);
        registerTable.getHeader().setStretchToFitActive (true);
        
        //hide the key column
        registerTable.getHeader().setColumnVisible (1, false);
        minAttendanceTime = studentRegister_.getMinAttendanceTime();
        maxAttendanceTime = studentRegister_.getMaxAttendanceTime();
        totalWeeksSoFar = ((maxAttendanceTime - minAttendanceTime) / timeInAWeek) + 1; //add some space at the end to accomadate for the non absolute

    }
    /** destruct */
    ~RegisterTable()
    {
        
    }
    
    //Component
    void resized()
    {
        registerTable.setBounds (getLocalBounds());
    }
    
    //TableListBoxModel
    int getNumRows() override
    {
        return studentRegister.getValueTree().getNumChildren();
    }
    
    void paintRowBackground (Graphics& g, int rowNumber, int width, int height, bool rowIsSelected) override
    {
        if (rowIsSelected)
            g.fillAll (Colours::lightblue);
        else if (rowNumber % 2)
            g.fillAll (Colour (0xffeeeeee));
    }

    void paintCell (Graphics& g, int rowNumber, int columnId, int width, int height, bool rowIsSelected) override
    {
        g.setColour (Colours::black);

        const Student student (studentRegister.getValueTree().getChild (rowNumber));

        if (columnId < 5)
        {
            DBG (student.studentTree.getPropertyName (columnId - 1).toString());
            String text (student.studentTree.getProperty (student.studentTree.getPropertyName (columnId - 1)).toString());

            g.drawText (text, 2, 0, width - 4, height, Justification::centredLeft, true);
        }
        else if (columnId == 6) {
            int total = student.studentTree.getChildWithName(Student::Ids::ATTENDANCERECORD).getNumChildren();
            String text = String(total) + String("/") + String(totalWeeksSoFar);
            g.drawText (text, 2, 0, width - 4, height, Justification::centredLeft, true);
            

        }
        g.setColour (Colours::black.withAlpha (0.2f));
        g.fillRect (width - 1, 0, 1, height);

    }

    // This is overloaded from TableListBoxModel, and must update any custom components that we're using
    Component* refreshComponentForCell (int rowNumber, int columnId, bool /*isRowSelected*/,
                                        Component* existingComponentToUpdate) override
    {
        if (columnId == 5)
        {
            AttendanceDisplay* attendanceDisplay = (AttendanceDisplay*) existingComponentToUpdate;
            
            if (attendanceDisplay != 0) {
                delete attendanceDisplay;
            }
            
            //unfortantly we need to make a new component each time as it will make it far easier to update
            ValueTree studentTree (studentRegister.getValueTree().getChild (rowNumber));
            attendanceDisplay = new AttendanceDisplay (*this, studentTree.getChildWithName(Student::Ids::ATTENDANCERECORD), rowNumber, minAttendanceTime, totalWeeksSoFar);

            return attendanceDisplay;
        }//comment this back in as it will stop keys from being edited. which is handy for testing

        else if (/*columnId == 1 ||*/ columnId > 5) // The ID and Length columns do not have a custom component
        {
            jassert (existingComponentToUpdate == 0);
            return 0;
        }


//        else if (columnId == 5) // For the ratings column, we return the custom combobox component
//        {
//            RatingColumnCustomComponent* ratingsBox = (RatingColumnCustomComponent*) existingComponentToUpdate;
//            
//            // If an existing component is being passed-in for updating, we'll re-use it, but
//            // if not, we'll have to create one.
//            if (ratingsBox == 0)
//                ratingsBox = new RatingColumnCustomComponent (*this);
//                
//                ratingsBox->setRowAndColumn (rowNumber, columnId);
//                
//                return ratingsBox;
//        }
        else // The other columns are editable text columns, for which we use the custom Label component
        {
            EditableTextCustomComponent* textLabel = (EditableTextCustomComponent*) existingComponentToUpdate;
            if (textLabel != 0) {
                delete textLabel;
            }
 
            ValueTree studentTree (studentRegister.getValueTree().getChild (rowNumber));
            Value value (studentTree.getPropertyAsValue (studentTree.getPropertyName (columnId - 1), nullptr));
            textLabel = new EditableTextCustomComponent (*this, value, rowNumber);
            return textLabel;
        }
    }

private:
    //==============================================================================
    // This is a custom Label component, which we use for the table's editable text columns.
    class EditableTextCustomComponent : public Label
    {
    public:
        EditableTextCustomComponent (RegisterTable& owner_, Value& valueToReferTo, int row_)
         :  owner (owner_), row (row_)
        {
            // double click to edit the label text; single click handled below
            setEditable (false, true, false);
            setColour (textColourId, Colours::black);
            getTextValue().referTo (valueToReferTo);
        }
        
        void mouseDown (const MouseEvent& event) override
        {
            // single click on the label should simply select the row
            owner.registerTable.selectRowsBasedOnModifierKeys (row, event.mods, false);
            Label::mouseDown (event);
        }
        
    private:
        RegisterTable& owner;
        int row;
    };
    /** Class to visualise the year the width is split into 52 weeks and a number is
     displayed at the end */
    class AttendanceDisplay : public Component
    {
    public:
        /** construct */
        AttendanceDisplay (RegisterTable& owner_, ValueTree attendanceTree, int row_, int64 minT, int64 nbrOfWeeksSoFar_)
         :  owner (owner_),
            tree (attendanceTree),
            row (row_),
            minAttendanceTime(minT),
            nbrOfWeeksSoFar(nbrOfWeeksSoFar_)
    
        {
            DBG (tree.getNumChildren());
                    }
        /** destruct */
        ~AttendanceDisplay() {}
        
        //Component
        void mouseDown (const MouseEvent& event) override
        {
            // single click on the label should simply select the row
            owner.registerTable.selectRowsBasedOnModifierKeys (row, event.mods, false);
        }
        void paint (Graphics& g)
        {
            float weekWidth = getWidth() / (float) nbrOfWeeksSoFar;
            float height = getHeight();
            
            int marks[nbrOfWeeksSoFar]; //store a simple tally
            memset(marks, 0, sizeof(int) * nbrOfWeeksSoFar); //cant init marks with = {0};
            
            const int numChildren = tree.getNumChildren();
            for (int i = 0; i < numChildren; ++i) {
                int64 time = tree.getChild(i).getProperty(Student::Ids::Time, Time::getCurrentTime().toMilliseconds());
                const int week = (time - minAttendanceTime) / timeInAWeek;
                jassert(week >= 0 && week < nbrOfWeeksSoFar);
                marks[week] = 1;
            }

            for (int i = 0; i <= nbrOfWeeksSoFar; ++i)
            {
                g.setColour(Colour::fromRGB(0, 0, 0));
                g.fillRect (i * weekWidth, 0.f, 1.f, height);
                if (i < nbrOfWeeksSoFar) { //sanity check..
                    if (marks[i]) {
                        g.setColour(Colour::fromRGB(53, 210, 25));
                        g.fillRect (3.f + ((float)i * weekWidth), 3.f, weekWidth - 6.f, height - 6.f);
                    }
                }
            }
        }
    private:
        RegisterTable& owner;
        ValueTree tree;
        int row;
        int64 minAttendanceTime;
        int64 nbrOfWeeksSoFar;
    };

    Register& studentRegister;
    TableListBox registerTable;
    int64 maxAttendanceTime;
    int64 minAttendanceTime;
    int64 totalWeeksSoFar;
};

/** Contains all the components for displaying the register */
class RegisterComponent :   public Component
{
public:
    /** construct */
    RegisterComponent (Register& studentRegister_) : registerTable (studentRegister_)
    {
        addAndMakeVisible (registerTable);
    }
    /** destruct */
    ~RegisterComponent() {}
    
    //Component
    void resized() override
    {
        registerTable.setBounds (getLocalBounds());
    }
    
private:
    
    RegisterTable registerTable;
};


#endif  // REGISTERCOMPONENT_H_INCLUDED
